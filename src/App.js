import React from "react";
import logo from "./assets/logo.svg";
import "./App.scss";
import { UserDetails } from "./UserDetails";
import { SelectedUser } from "./User";
import { CardDetails } from "./CardDetails";
import { Transactions } from "./Transactions";

function App() {
  return (
    <div className="app">
      <header className="header">
        <img src={logo} className="logo" alt="logo" />
        <h4>Dashboard</h4>
        <UserDetails />
      </header>
      <div className="wrapper">
        <section className="widget widget__small">
          <div>
            <SelectedUser />
          </div>
          <div>
            <CardDetails />
          </div>
        </section>
        <section className="widget">
          <Transactions />
        </section>
      </div>
    </div>
  );
}

export default App;
