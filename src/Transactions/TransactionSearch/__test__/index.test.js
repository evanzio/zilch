import React from 'react';
import { render, screen } from '@testing-library/react'
import TransactionSearch from '../index.js';

test('has correct input value', () => {
  render(<TransactionSearch />)
  expect(screen.getByLabelText('Search for payment', { selector: 'input' })
)})
