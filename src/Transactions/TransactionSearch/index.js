import React from "react";

const TransactionSearch = ({ searchTerm, handleChange }) => {
  return (
    <form name="search transaction" className="form__group field">
      <input
        id="Search"
        className="form__field"
        type="input"
        placeholder="Search"
        defaultValue={searchTerm}
        onChange={handleChange}
      />
      <label htmlFor="Search" className="form__label">
        Search for payment
      </label>
    </form>
  );
};

export default TransactionSearch;
