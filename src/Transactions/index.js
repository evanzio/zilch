import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import TransactionRow from "./TransactionRow";
import TransactionSearch from "./TransactionSearch";

const Transactions = () => {
  const dispatch = useDispatch();
  const transactions = useSelector((state) => state.transactions.transactions);
  const userId = useSelector((state) => state.selectedUser.user);

  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState("");

  const handleChange = (e) => {
    setSearchTerm(e.target.value);
  };

  useEffect(() => {
    const results = (transactions || []).filter((item) =>
      item.company.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSearchResults(results);
  }, [searchTerm, transactions]);

  useEffect(() => {
    dispatch({ type: "GET_TRANSACTIONS_REQUESTED", userId: userId.id });
  }, [dispatch, userId]);

  return (
    <div>
      <div className="transaction__header">
        <div className="header__title">Transactions</div>
        <TransactionSearch
          searchTerm={searchTerm}
          handleChange={handleChange}
        />
      </div>
      <div className="row transaction__titles">
        <div className="cell">ID</div>
        <div className="cell">Store</div>
        <div className="cell">Date</div>
        <div className="cell">Cost</div>
      </div>
      <div className="transactions">
        {transactions &&
          searchResults.map((transaction) => (
            <TransactionRow key={transaction.id} transaction={transaction} />
          ))}
      </div>
    </div>
  );
};

export { Transactions };
