import React from 'react';
import { render, screen } from '@testing-library/react'
import TransactionRow from '../index.js';

const mockData = {
  id: 1,
  company: "ASOS",
  date: "12/03/2020",
  cost: 322
}

test('has correct transaction details', () => {
  render(<TransactionRow key={mockData.id} transaction={mockData} />)
  expect(screen.getByText('ASOS')).toHaveTextContent('ASOS')
})
