import React from 'react'

const TransactionRow = ({transaction}) => {
  return (
    <div className="row">
      <div className="cell">{transaction.id}</div>
      <div className="cell">{transaction.company}</div>
      <div className="cell">{transaction.date}</div>
      <div className="cell">£ {transaction.cost}</div>
    </div>
  )
}

export default TransactionRow;
