import { call, put, takeEvery} from 'redux-saga/effects';

const apiUrl = 'http://localhost:4000/'

function getApi(val) {
  let updatedApiUrl = apiUrl + 'transactions/?userId=' + val
  return fetch(updatedApiUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(response => response.json())
    .catch((error) => {throw error})
}

function* fetchTransactions(action) {
  try {
    const transactions = yield call(getApi, action.userId);
    yield put({type: 'GET_TRANSACTIONS_SUCCESS', transactions: transactions});
  } catch (e) {
    yield put({ type: 'GET_TRANSACTIONS_FAILED', message: e.message })
  }
}

function* transactionsSaga() {
  yield takeEvery('GET_TRANSACTIONS_REQUESTED', fetchTransactions)
}

export default transactionsSaga;
