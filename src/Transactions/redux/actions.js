import {createActions} from 'redux-actions';

const types = {
  GET_TRANSACTIONS_REQUESTED: 'GET_TRANSACTIONS_REQUESTED',
  GET_TRANSACTIONS_SUCCESS: 'GET_TRANSACTIONS_SUCCESS',
  GET_TRANSACTIONS_FAILED: 'GET_TRANSACTIONS_FAILED',
};

const actions = createActions(...Object.keys(types).map((type) => types[type]));

export default actions;
export {types};
