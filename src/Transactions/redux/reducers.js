import { handleActions } from 'redux-actions';
import { types } from './actions';

const transactions = handleActions(
  {
    [types.GET_TRANSACTIONS_REQUESTED](state, action) {
      return {
        ...state,
        isLoading: true,
        hasError: false,
        userId: action.userId
      };
    },
    [types.GET_TRANSACTIONS_SUCCESS](state, action) {
      return {
        ...state,
        isLoading: false,
        hasError: false,
        transactions: action.transactions,
      };
    },
    [types.GET_TRANSACTIONS_FAILED](state) {
      return {...state, isLoading: false, hasError: true,};
    },
  },
  {
    isLoading: false,
    hasError: false,
    userId: 1,
    transactions: null
  },
);

export default transactions;
