import React from 'react';
import { render, screen } from '@testing-library/react'
import {UserDetails, Option} from '../index.js';
import { Provider } from 'react-redux';
import store from '../../redux/store';


test('renders without crashing', () => {
  render(
    <Provider store={store}>
      <UserDetails />
    </Provider>,
  );

  expect(screen.getByTestId('select-user')).toBeInTheDocument();
});

const mockData = {
  id: 1,
  firstName: "John",
  lastName: "Doe",
}

test('has correct firstName text', () => {
  render(<Option key={mockData.id} data={mockData} />)
  expect(screen.getByRole('option')).toHaveTextContent('John')
})
