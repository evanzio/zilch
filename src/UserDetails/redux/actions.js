import {createActions} from 'redux-actions';

const types = {
  GET_USERS_REQUESTED: 'GET_USERS_REQUESTED',
  GET_USERS_SUCCESS: 'GET_USERS_SUCCESS',
  GET_USERS_FAILED: 'GET_USERS_FAILED',
};

const actions = createActions(...Object.keys(types).map((type) => types[type]));

export default actions;
export {types};
