import { handleActions } from 'redux-actions';
import { types } from './actions';

const userDetails = handleActions(
  {
    [types.GET_USERS_REQUESTED](state, action) {
      return {
        ...state,
        isLoading: true,
        hasError: false,
      };
    },
    [types.GET_USERS_SUCCESS](state, action) {
      return {
        ...state,
        isLoading: false,
        hasError: false,
        users: action.users,
      };
    },
    [types.GET_USERS_FAILED](state, e) {
      return {...state, isLoading: false, hasError: true, message: e.message};
    },
  },
  {
    isLoading: false,
    hasError: false,
    users: null,
    message: null,
  },
);

export default userDetails;
