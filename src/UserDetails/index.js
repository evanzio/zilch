import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

const Option = ({data}) => {
  return <option value={JSON.stringify(data)}>{data.firstName}</option>
}
export { Option }

const UserDetails = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.userDetails.users);

  const handleChange = (val) => {
    dispatch({ type: "SELECTED_USER", user: JSON.parse(val) });
  };

  useEffect(() => {
    dispatch({ type: "GET_USERS_REQUESTED" });
  }, [dispatch]);

  return (
    <select
      className="select__user"
      data-testid="select-user"
      onChange={(event) => handleChange(event.target.value)}
    >
      {users &&
        users.map((user) => (
          <Option key={user.id} data={user} />
        ))}
    </select>
  );
};

export { UserDetails };
