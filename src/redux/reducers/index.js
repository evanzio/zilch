import {combineReducers} from 'redux';
import userDetails from '../../UserDetails/redux/reducers';
import selectedUser from '../../User/redux/reducers';
import cardDetails from '../../CardDetails/redux/reducers';
import transactions from '../../Transactions/redux/reducers';

const rootReducer = combineReducers({
  userDetails,
  selectedUser,
  cardDetails,
  transactions,
});

export default rootReducer;
