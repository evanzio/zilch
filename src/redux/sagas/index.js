import { all } from 'redux-saga/effects'
import userSaga from '../../UserDetails/redux/saga';
import cardDetailsSaga from '../../CardDetails/redux/saga';
import transactionsSaga from '../../Transactions/redux/saga';

export default function* rootSaga() {
  yield all([
    userSaga(),
    cardDetailsSaga(),
    transactionsSaga(),
  ])
}
