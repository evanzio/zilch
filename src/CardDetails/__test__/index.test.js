import React from 'react';
import { render, screen } from '@testing-library/react'
import {Card} from '../index.js';

const mockData = {
  id: 1,
  cardNumber: '1223 3333 2232 9984',
  expiryDate: '12/12',
  cvv: '344',
}
test('has correct card number', () => {
  render(<Card key={mockData.id} details={mockData} />)
  expect(screen.getByText('1223 3333 2232 9984')).toHaveTextContent('1223 3333 2232 9984')
  expect(screen.getByText('12/12')).toHaveTextContent('12/12')
  expect(screen.getByText('344')).toHaveTextContent('344')
})
