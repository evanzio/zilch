import { handleActions } from 'redux-actions';
import { types } from './actions';

const cardDetails = handleActions(
  {
    [types.GET_CARD_DETAILS_REQUESTED](state, action) {
      return {
        ...state,
        isLoading: true,
        hasError: false,
        userId: action.userId,
      };
    },
    [types.GET_CARD_DETAILS_SUCCESS](state, action) {
      return {
        ...state,
        isLoading: false,
        hasError: false,
        cardDetails: action.cardDetails,
      };
    },
    [types.GET_CARD_DETAILS_FAILED](state) {
      return {...state, isLoading: false, hasError: true,};
    },
  },
  {
    isLoading: false,
    hasError: false,
    userId: 1,
    cardDetails: null,
  },
);

export default cardDetails;
