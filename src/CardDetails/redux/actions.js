import {createActions} from 'redux-actions';

const types = {
  GET_CARD_DETAILS_REQUESTED: 'GET_CARD_DETAILS_REQUESTED',
  GET_CARD_DETAILS_SUCCESS: 'GET_CARD_DETAILS_SUCCESS',
  GET_CARD_DETAILS_FAILED: 'GET_CARD_DETAILS_FAILED',
};

const actions = createActions(...Object.keys(types).map((type) => types[type]));

export default actions;
export {types};
