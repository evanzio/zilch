import { call, put, takeEvery} from 'redux-saga/effects';

const apiUrl = 'http://localhost:4000/'

function getApi(val) {
  let updatedApiUrl = apiUrl + 'cardDetails/?userId=' + val
  return fetch(updatedApiUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(response => response.json())
    .catch((error) => {throw error})
}


function* fetchCardDetails(action) {
  try {
    const cardDetails = yield call(getApi, action.userId);
    yield put({type: 'GET_CARD_DETAILS_SUCCESS', cardDetails: cardDetails});
  } catch (e) {
    yield put({ type: 'GET_CARD_DETAILS_FAILED', message: e.message })
  }
}

function* cardDetailsSaga() {
  yield takeEvery('GET_CARD_DETAILS_REQUESTED', fetchCardDetails)
}

export default cardDetailsSaga;
