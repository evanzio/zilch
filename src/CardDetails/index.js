import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

const Card = ({details}) => {
  return (
    <div className="card__container">
      <div className="card">
        <div className="card__number">{details.cardNumber}</div>
        <div className="card__expiry">Exp: <span>{details.expiryDate}</span></div>
        <div className="card__csv">CVV: <span>{details.cvv}</span></div>
      </div>
      <div className="creditLimit">Credit Limit: £{details.creditLimit}</div>
    </div>
  )
}

const CardDetails = () => {
  const dispatch = useDispatch();
  const cardDetails = useSelector(state => state.cardDetails.cardDetails);
  const user = useSelector(state => state.selectedUser.user);

  useEffect(() => {
    dispatch({ type: 'GET_CARD_DETAILS_REQUESTED', userId: user.id });
  }, [dispatch, user]);

  return (
    cardDetails && cardDetails.map((deets) => (
      <Card key={deets.id} details={deets} />
    ))
  )
}

export { CardDetails, Card };
