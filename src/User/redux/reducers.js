import { handleActions } from 'redux-actions';
import { types } from './actions';

const selectedUser = handleActions(
  {
    [types.SELECTED_USER](state, action) {
      return {
        ...state,
        user: action.user,
      };
    },
  },
  {
    user: { id: 1, firstName: "Ira", lastName: "Flynn", email: "laoreet@Curabiturconsequatlectus.org"},
  },
);

export default selectedUser;
