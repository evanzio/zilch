import {createActions} from 'redux-actions';

const types = {
  SELECTED_USER: 'SELECTED_USER',
}

const actions = createActions(...Object.keys(types).map((type) => types[type]));

export default actions;
export {types};
