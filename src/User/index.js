import React from "react";
import { useSelector } from "react-redux";

const Name = ({ firstName, lastName }) => {
  return (
    <div className="header__title">
      <span>{firstName} {lastName}</span>
    </div>
  );
};

const SelectedUser = () => {
  const user = useSelector((state) => state.selectedUser.user);
  return <Name firstName={user.firstName} lastName={user.lastName} />;
};
export { SelectedUser, Name };
