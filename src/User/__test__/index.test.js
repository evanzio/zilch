import React from 'react';
import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux';
import {SelectedUser, Name} from '../index.js';
import store from '../../redux/store';


test('renders without crashing', () => {
  render(<Provider store={store}>
          <SelectedUser />
        </Provider>);
  expect(screen.getByText(/ira/i)).toBeInTheDocument();
});


test('has correct welcome text', () => {
  render(<Name firstName="John" lastName="Doe" />)
  expect(screen.getByText('John Doe')).toHaveTextContent('John Doe')
})
