
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Tech task

- Task to built  React app utilising React / Redux / Saga libraries including an API.
- Select a user from the drop down and the users card details are displayed and their personal transactions are also displayed.
- You are then able to filter the transactions with the 'Search for payment' input.

## Prerequisites for api json server

Install json-server globally

[JSON server Github Page](https://github.com/typicode/json-server)

`npm install -g json-server`

In the project directory navigate to `json-server` directory and run the following command in terminal:

`json-server --watch db.json --port 4000`


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.
